﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    bool activationState;
    public bool ActivationState
    {
        get
        {
            return activationState;
        }
        set
        {
            Time.timeScale = value ? 0 : 1;

            Cursor.lockState = value ? CursorLockMode.None : CursorLockMode.Confined;

            Objects.SetActive(value);

            activationState = value;
        }
    }

    [SerializeField] GameObject Objects;

    [SerializeField] AudioMixer mixer;

    [SerializeField] Slider VolumeSlider;
    
    [SerializeField] TMP_Dropdown languageDropdown;

    [SerializeField] myInt language;

    private CharaInputs m_charaInputs;

    private void Start()
    {
        m_charaInputs = new CharaInputs();

        float f = 0;
        mixer.GetFloat("master", out f);
        VolumeSlider.value = Mathf.Pow(10, (f / 20));
        languageDropdown.value = language.m_int;
        languageDropdown.onValueChanged.AddListener(delegate { SelectLanguage(languageDropdown); });
    }

    private void Update()
    {
        if (m_charaInputs.pause.WasPressed)
            ActivationState = !ActivationState;
    }

    public void SelectLanguage(TMP_Dropdown change)
    {
        language.m_int = change.value;
    }

    public void SetVolume(float sliderValue)
    {
        mixer.SetFloat("master", Mathf.Log10(sliderValue) * 20);
    }

    public void Quit()
    {
        ActivationState = false;
        Application.Quit();
    }
}
