﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using TMPro;

public class Menu : MonoBehaviour
{
    [SerializeField] AudioMixer mixer;

    [SerializeField] GameObject SettingsOptions;

    [SerializeField] Image blackScreen;

    [SerializeField] TMP_Dropdown m_Dropdown;

    [SerializeField] Slider VolumeSlider;

    [SerializeField] myInt language;


    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        blackScreen.DOFade(0, 1).SetEase(Ease.Linear);
        m_Dropdown.onValueChanged.AddListener(delegate { SelectLanguage(m_Dropdown); });
        language.m_int = 0;

        float f = 0;
        mixer.GetFloat("master", out f);
        VolumeSlider.value = Mathf.Pow(10, (f / 20));
    }

    public void Play()
    {
        Cursor.lockState = CursorLockMode.Confined;

        SceneManager.LoadSceneAsync(Constants.k_CinematicStart);

        blackScreen.DOFade(1, 1).SetEase(Ease.Linear);     
    }

    public void Settings()
    {
        SettingsOptions.SetActive(!SettingsOptions.activeInHierarchy);
    }

    public void SelectLanguage(TMP_Dropdown change)
    {
        language.m_int = change.value;
    }

    public void SetVolume(float sliderValue)
    {
        mixer.SetFloat("master", Mathf.Log10(sliderValue) * 20);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
