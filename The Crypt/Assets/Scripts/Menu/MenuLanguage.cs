﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuLanguage : MonoBehaviour
{
    public myInt language;
    public string[] texts = new string[0];

    TextMeshProUGUI tmp;

    private void Start()
    {
        tmp = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        tmp.text = texts[language.m_int];
    }
}
