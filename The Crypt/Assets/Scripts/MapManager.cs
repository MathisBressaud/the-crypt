﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class MapManager : Singleton<MapManager>
{
    [Header("Test")]
    public bool TESTMOD;
    public Floor[] floorsTest = new Floor[0];

    CharaControler cC;
    PlayerEntity pEntity;

    int currentFloor;
    int CurrentFloor
    {
        get { return currentFloor; }
        set
        {
            if (currentFloor == value)
                return;

            currentFloor = value;
            TweenFloors(TESTMOD ? floorsTest : floors);
        }
    }


    [Header("For The Player")]
    public Ladder respawn;
    [Space(10)]
    public float timeToRespawn;




    [Header("Floor params")]
    public FloorParams fParams;
    public float timeToTweenFloor;
    [Space(10)]
    public Floor[] floors = new Floor[0];

	private AudioSource m_ambiantSource;


    bool timeFreeze;
    public bool TimeFreeze
    {
        get { return timeFreeze; }
        set
        {
            if (timeFreeze == value)
                return;

            if(cC)
                cC.IsFreeze = value;


            if (value)
                StartCoroutine(Delay());


            timeFreeze = value;
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(timeToRespawn);
        if (TimeFreeze)
            PlayerRespawn(true);
    }



    private void Start()
    {
        cC = FindObjectOfType<CharaControler>();
        pEntity = FindObjectOfType<PlayerEntity>();

        //PlayerRespawn();
        CurrentFloor = 0;

        TweenFloors(TESTMOD ? floorsTest : floors);
    }

    public void SwitchFloor(int dir)
    {
        CurrentFloor += dir;
    }

    void TweenFloors(Floor[] tweenedFloor)
    {
        AudioManager.instance.PlaySFXOnce(AudioNames.Transition_Floor, 8);

        //if(CurrentFloor == 0)
        //{
        if (m_ambiantSource == null)
        {
            m_ambiantSource = AudioManager.instance.PlaySFXLoop(AudioNames.Cimetierre_Ambiance, 0.1f);
        }
        //	else
        //	{
        //		AudioManager.instance.PlaySFXWithSource(AudioNames.Cimetierre_Ambiance, m_ambiantSource);
        //	}
        //}
        //else
        //{
        //	AudioManager.instance.PlaySFXWithSource(AudioNames.Cave_Ambiance, m_ambiantSource);
        //}

        for (int i = 0; i < tweenedFloor.Length; i++)
        {
            //le new play floor
            if(i == CurrentFloor)
            {
                TimeFreeze = true;
                int val = i;

                tweenedFloor[i].floor.DOMoveY(fParams.playFloorHigh, timeToTweenFloor).SetEase(Ease.InOutQuad).Play().OnComplete(() =>
                {
                    TimeFreeze = false;
                    tweenedFloor[val].enableIfPlayGround.SetActive(true);
                    tweenedFloor[val].enableIfNotCompact.SetActive(true);
                });
            }
            //le new floor du dessous
            else if (i == CurrentFloor + 1)
            {
                tweenedFloor[i].enableIfPlayGround.SetActive(false);
                int val = i;
                tweenedFloor[i].floor.DOMoveY(fParams.bottomFloorHigh, timeToTweenFloor).SetEase(Ease.InOutQuad).Play().OnComplete(() =>
                {
                    tweenedFloor[val].enableIfNotCompact.SetActive(true);
                });
            }
            //le new floor du dessus
            else if (i == CurrentFloor - 1)
            {
                tweenedFloor[i].enableIfPlayGround.SetActive(false);
                int val = i;
                tweenedFloor[i].floor.DOMoveY(fParams.topFloorHigh, timeToTweenFloor).SetEase(Ease.InOutQuad).Play().OnComplete(() =>
                {
                    tweenedFloor[val].enableIfNotCompact.SetActive(true);
                });
            }
            //les autres
            else
            {
                tweenedFloor[i].enableIfNotCompact.SetActive(false);
                int yHigh = (i < CurrentFloor ? (CurrentFloor - 1 - i) * fParams.compactFloorHigh + fParams.topFloorHigh : (CurrentFloor + 1 - i) * fParams.compactFloorHigh + fParams.bottomFloorHigh);
                tweenedFloor[i].floor.DOMoveY(yHigh, timeToTweenFloor).SetEase(Ease.InOutQuad).Play();

            }
        }
    }

    public FloorState FindFloorStateAtPosition(Vector3 position)
    {
        if (position.y >= fParams.playFloorHigh && position.y < fParams.topFloorHigh)
        {
            return FloorState.playable;
        }
        else if (position.y >= fParams.bottomFloorHigh && position.y < fParams.playFloorHigh)
        {
            return FloorState.peacefull;
        }
        else if (position.y >= fParams.topFloorHigh && position.y < fParams.topFloorHigh + fParams.compactFloorHigh)
        {
            return FloorState.peacefull;
        }
        else
        {
            return FloorState.compact;
        }
    }

    public void ValidateRespawn(Ladder ladder)
    {
        respawn = ladder;
    }

    public void PlayerRespawn(bool forcing = false)
    {
        if (TimeFreeze && !forcing)
            return;

        TimeFreeze = true;
        Vector3 resPos = respawn ? respawn.transform.position : transform.position;

        if(respawn)
            respawn.Used = true;

        Fader.instance.TweenMoiCetEcran(timeToRespawn);

        cC.transform.DOMove(cC.transform.position, 0.14f).SetEase(Ease.InOutQuad).Play().OnComplete(() =>
        {
            cC.transform.position = resPos;
            cC.transform.DOMove(resPos, timeToRespawn).SetEase(Ease.InOutQuad).Play().OnComplete(() =>
            {
                pEntity.HP = pEntity.MAX_HP;
                TimeFreeze = false;
            });
        });
    }
}

[System.Serializable]
public class Floor
{
    public Transform floor;

    public GameObject enableIfPlayGround;
    public GameObject enableIfNotCompact;

    FloorState state;
    public FloorState State
    {
        get { return state; }
        set
        {
            if (state == value)
                return;

            state = value;
        }
    }
}

[System.Serializable]
public class FloorParams
{
    public int playFloorHigh;
    public int bottomFloorHigh;
    public int topFloorHigh;
    public int compactFloorHigh;
}

[System.Serializable] 
public enum FloorState
{
    playable,
    peacefull,
    compact,
}