﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Fader : Singleton<Fader>
{
    public Image fade2Black;

    public void TweenMoiCetEcran(float time)
    {
        fade2Black.DOFade(1, 0.14f).SetEase(Ease.InOutQuad).Play().OnComplete(() => {
            fade2Black.DOFade(1, time).SetEase(Ease.InOutQuad).Play().OnComplete(() => {
                fade2Black.DOFade(0, 0.14f).SetEase(Ease.InOutQuad).Play();
            });
        });
    }
}
