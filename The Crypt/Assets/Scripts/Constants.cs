﻿public class Constants
{
    //Scene
	public const string k_GameScene = "Level_0";
	public const string k_MenuScene = "Menu";
	public const string k_CinematicStart = "Cinematic_Intro";
	public const string k_CinematicEnd = "Cinematic_Fin";

    //Layer
    public const string k_LayerHitable = "Hitable";

    //Fire
    public const float k_FirePropagationRange = 2f;
    public const float k_FirePropagationRate = 0.5f;
    public const float k_FirePropagationChance = 20f;
    public const int k_FireDamage = 1;
    public const float k_FireDamageRate = 1f;

	//Fix
	public const float k_EntityGlobalInvulnerableTime = 0.2f;
}
