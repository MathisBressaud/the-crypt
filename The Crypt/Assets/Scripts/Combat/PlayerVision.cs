﻿using UnityEngine;

public class PlayerVision : MonoBehaviour
{
    [SerializeField] private float m_VisionAngle = 145f;
    [SerializeField] private float m_VisionRange = 20f;

    public bool CanSee(Vector3 position)
    {
        Vector3 dir = position - transform.position;

        return Vector3.Angle(transform.forward, dir) <= (m_VisionAngle / 2f) && dir.magnitude <= m_VisionRange;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + (Quaternion.AngleAxis(m_VisionAngle / 2f, Vector3.up) * transform.forward) * m_VisionRange);
        Gizmos.DrawLine(transform.position, transform.position + (Quaternion.AngleAxis(-m_VisionAngle / 2f, Vector3.up) * transform.forward) * m_VisionRange);
    }
}
