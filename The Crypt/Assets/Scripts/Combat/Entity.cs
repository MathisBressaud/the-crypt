﻿using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using DG.Tweening;

public class Entity : MonoBehaviour, IHealth, ICombat
{
    [SerializeField, TitleGroup("Entity")] private int hp = 3;
    public int HP
    {
        get
        {
            return hp;
        }
        set
        {
            if (value < hp)
            {
                GetHit();
            }

            hp = value;

            if (hp <= 0)
            {
                Death();
            }
        }
    }

    [SerializeField, TitleGroup("Entity")] private int max_hp = 3;
    public int MAX_HP
    {
        get
        {
            return max_hp;
        }
        set
        {
            max_hp = value;
        }
    }

    [SerializeField, TitleGroup("Entity")] private Side side;
    public Side SIDE
    {
        get
        {
            return side;
        }
    }

	[Space]
	[SerializeField, TitleGroup("Entity")] private bool m_HasRagdoll;
	[TitleGroup("Entity"), ShowIf("m_HasRagdoll")] public Pool m_RagdollPool;
    [TitleGroup("Entity"), ShowIf("m_HasRagdoll")] public float m_RagdollForceMultiplier = 35f;
    private Vector3 m_lastPush = Vector3.zero;
	protected float m_lastForce = 0f;

    [Space]
    [SerializeField, TitleGroup("Entity")] private UnityEvent m_OnDamaged;
    [SerializeField, TitleGroup("Entity")] private UnityEvent m_OnDestroyed;

	private bool m_isInvulnerable;
	private float m_endInvulnerableTime = 0;
    

    protected virtual void Start()
    {
        HP = MAX_HP;
		m_isInvulnerable = false;
    }

	private void LateUpdate()
	{
		if(m_isInvulnerable && Time.time >= m_endInvulnerableTime)
		{
			m_isInvulnerable = false;
		}
	}

    protected virtual void GetHit()
    {
        m_OnDamaged?.Invoke();
    }

    public virtual void GetDamage(int amount, Vector3 direction, float force)
    {
		if(!m_isInvulnerable)
		{
        	HP -= amount;

			m_lastPush = direction;
			m_lastForce = force;

			m_isInvulnerable = true;
			m_endInvulnerableTime = Time.time + Constants.k_EntityGlobalInvulnerableTime;
		}
    }

    protected virtual void Death()
    {
		if(m_HasRagdoll)
		{
			GameObject ragdoll = m_RagdollPool.GetAvailableMask();

			ragdoll.SetActive(false);

			ragdoll.transform.position = transform.position;
			ragdoll.transform.rotation = transform.rotation;
			ragdoll.transform.parent = transform.parent;

			ragdoll.transform.GetChild(0).transform.localPosition = Vector3.zero;

			var allRBGD = ragdoll.GetComponentsInChildren<Rigidbody>();

			for(int i = 0; i < allRBGD.Length; i++)
			{
				allRBGD[i].velocity = Vector3.zero;
			}

			ragdoll.SetActive(true);

			if(m_lastForce > 0)
			{
                Vector3 pushForce = m_lastPush.Flat().normalized * m_lastForce * m_RagdollForceMultiplier;

                for(int i = 0; i < allRBGD.Length; i++)
                {
					allRBGD[i].AddForce(pushForce);
                }
			}
		}

        m_OnDestroyed?.Invoke();
        gameObject.SetActive(false);
    }
}
