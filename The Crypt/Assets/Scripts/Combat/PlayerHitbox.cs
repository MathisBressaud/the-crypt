﻿using UnityEngine;

public class PlayerHitbox : Hitbox
{
    private int Damage
    {
        get { return PlayerSkills.instance.dmg; }
    }

    private float Force
    {
        get { return PlayerSkills.instance.knockBackForce; }
    }

    protected override void OnTriggerEnter(Collider other)
    {
        IHealth health = other.GetComponent<IHealth>();
        if (health != null)
        {
            ICombat combat = other.GetComponent<ICombat>();
            if (combat != null && combat.SIDE != SIDE)
            {
                health.GetDamage(Damage, other.transform.position - transform.position, Force);
            }
            else if (combat == null)
            {
                health.GetDamage(Damage, other.transform.position - transform.position, Force);
            }
        }

        if(PlayerSkills.instance.Skills == Skills.Torch)
        {
            IFlamable flamable = other.GetComponent<IFlamable>();

            if(flamable!=null)
            {
                flamable.Lit();
            }
        }
    }
}
