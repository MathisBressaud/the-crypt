﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEntity : Entity
{
    protected override void Death()
    {
        MapManager.instance.PlayerRespawn();
        //m_OnDestroyed?.Invoke();
    }
}
