﻿using UnityEngine;

public class Hitbox : MonoBehaviour, ICombat
{
    [SerializeField] protected int m_damage = 1;

    [SerializeField] protected float m_force = 10;

    [SerializeField] protected Side side;
    public Side SIDE
    {
        get { return side; }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        IHealth health = other.GetComponent<IHealth>();
        if(health != null)
        {
            ICombat combat = other.GetComponent<ICombat>();
            if (combat != null && combat.SIDE != SIDE)
            {
                health.GetDamage(m_damage, other.transform.position - transform.position, m_force);
            }
            else if (combat == null)
            {
                health.GetDamage(m_damage, other.transform.position - transform.position, m_force);
            }
        }
    }
}
