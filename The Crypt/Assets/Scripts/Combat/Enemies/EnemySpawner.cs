﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class EnemySpawner : MonoBehaviour
{
    //References
    [SerializeField, TitleGroup("Spawner")] private Transform[] m_SpawnPoints;
    [Space]
    [SerializeField, TitleGroup("Spawner")] private Pool m_SpawnedObjectPool;	
	[SerializeField, TitleGroup("Spawner")] private Pool m_RagdollPool;

    //Variables
    [Space]
    [SerializeField, TitleGroup("Spawner")] private float m_SpawnRange = 20f;
    private Transform m_playerTransform;

    [SerializeField, TitleGroup("Spawner")] private int m_MaxSpawnedObjects = 5;
    private List<GameObject> m_spawnObjects = new List<GameObject>();

    [SerializeField, TitleGroup("Spawner")] private float m_MinTimeBetweenTwoSpawn = 1f;
    [SerializeField, TitleGroup("Spawner")] private float m_MaxTimeBetweenTwoSpawn = 2f;
    private float m_timeBeforeNextSpawn = 0;

	//Events
	[Space]
	[SerializeField, TitleGroup("Spawner")] private UnityEvent m_OnSpawn;

    //Public bools
    public bool IsActive { get; set; }

    private bool m_playerDetected;
    public bool PlayerDetected
    {
        get
        {
			bool currentValue = Vector3.Distance(m_playerTransform.position, transform.position) <= m_SpawnRange && MapManager.instance.FindFloorStateAtPosition(transform.position) == FloorState.playable;

            if(currentValue && !m_playerDetected)
            {
                m_timeBeforeNextSpawn = Time.time + Random.Range(m_MinTimeBetweenTwoSpawn, m_MaxTimeBetweenTwoSpawn);
            }

            m_playerDetected = currentValue;

            return m_playerDetected;
        }
    }


    private void Start()
    {
        m_playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        //Test
        IsActive = true;
    }

    private void Update()
    {
        if (IsActive)
        {
            var tempList = m_spawnObjects;
            for (int i = 0; i < m_spawnObjects.Count; i++)
            {
                if(!m_spawnObjects[i].activeInHierarchy)
                {
                    tempList.Remove(m_spawnObjects[i]);
                }
            }
            m_spawnObjects = tempList;
            
            if (PlayerDetected && m_spawnObjects.Count < m_MaxSpawnedObjects && Time.time >= m_timeBeforeNextSpawn)
            {
                Spawn();
                m_timeBeforeNextSpawn = Time.time + Random.Range(m_MinTimeBetweenTwoSpawn, m_MaxTimeBetweenTwoSpawn);
            }
        }
    }

    public void Spawn()
    {
        //Choose Random SpawnPoint
        int rng = Random.Range(0, m_SpawnPoints.Length);
        Transform currentSpawnPoint = m_SpawnPoints[rng];

        //Place Enemy
        GameObject enemyToSpawn = m_SpawnedObjectPool.GetAvailableMask();

        enemyToSpawn.transform.position = currentSpawnPoint.transform.position;
        enemyToSpawn.transform.rotation = currentSpawnPoint.transform.rotation;
		enemyToSpawn.transform.parent = transform.parent;

        enemyToSpawn.GetComponent<Rigidbody>().velocity = Vector3.zero;
		enemyToSpawn.GetComponent<Entity>().m_RagdollPool = m_RagdollPool;

        m_spawnObjects.Add(enemyToSpawn);

        //Spawn
		m_OnSpawn?.Invoke();

        enemyToSpawn.SetActive(true);
        enemyToSpawn.GetComponent<EnemyPathfinder>().CanMove = true;
    }
}