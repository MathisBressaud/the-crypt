﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Sirenix.OdinInspector;
using DG.Tweening;

public class Vampire : Enemy
{
    //References
    [SerializeField, TitleGroup("Vampire")] private Cerceuil m_Cerceuil;
    [SerializeField, TitleGroup("Vampire")] private Collider m_Collider;
	[SerializeField, TitleGroup("Enemy")] private Hitbox m_AttackHitbox;

    //Variables
    [Space]
    [SerializeField, TitleGroup("Vampire")] private float m_AttackLockTime = 0.5f;
    [SerializeField, TitleGroup("Vampire")] private float m_AttackPreparationTime = 0.5f;
    [SerializeField, TitleGroup("Vampire")] private float m_AttackSpeed = 10f;
    [SerializeField, TitleGroup("Vampire")] private float m_AttackRecoveryTime = 0.1f;
    [SerializeField, TitleGroup("Vampire")] private float m_DashDepassement = 5f;
    private bool isAttacking;
    private Coroutine m_attackCoroutine;

	private bool isRespawning;

	private Tween m_dashTween;

    [Space]
    [SerializeField, TitleGroup("Vampire")] private float m_RespawnTime = 1;

	[Space]
	[SerializeField, TitleGroup("Vampire")] private LayerMask m_DashLayer;

    //Events
    [SerializeField, TitleGroup("Vampire")] private UnityEvent m_OnRespawn;

	protected override void Update()
	{
		if(!isRespawning)
		{
			base.Update();
		}

		if(!m_isInTransition && MapManager.instance.TimeFreeze)
		{
			StartCoroutine(Transition());
		}
	}

    protected override void Attack()
    {
        if(!isAttacking)
        {
            isAttacking = true;
            m_attackCoroutine = StartCoroutine(MegaDash());
        }
    }

    protected override void GetHit()
    {
        if(m_attackCoroutine != null)
        {
            StopCoroutine(m_attackCoroutine);
            isAttacking = false;
        }

        base.GetHit();
    }

    IEnumerator MegaDash()
    {
        //Preparation
        m_pathfinder.CanMove = false;

        //Sign + Anim Lock
        m_Animator.SetTrigger("Attack");

        transform.DOLookAt(m_pathfinder.m_Destination.position, m_AttackLockTime).Play();

		AudioManager.instance.PlaySFXOnce(AudioNames.Vampire_Load, 5);

        yield return new WaitForSeconds(m_AttackLockTime);

        transform.DOLookAt(m_pathfinder.m_Destination.position, m_AttackPreparationTime).Play();

        Vector3 destination = m_pathfinder.m_Destination.position;

        yield return new WaitForSeconds(m_AttackPreparationTime);

        m_Collider.enabled = false;
        m_AttackHitbox.gameObject.SetActive(true);

        //Dash
        m_Animator.SetBool("isDashing", true);

        Vector3 dir = (destination - transform.position).Flat();
        float dashTime = 1 / ((dir.magnitude + m_DashDepassement) * m_AttackSpeed);

		Vector3 endPos = destination + dir.normalized * m_DashDepassement;

		float portion = 1;

		RaycastHit hit;
		if(Physics.Raycast(transform.position, dir.normalized, out hit, dir.magnitude + m_DashDepassement, m_DashLayer))
		{
			portion = Mathf.Clamp01((hit.point - transform.position).magnitude / (dir.magnitude + m_DashDepassement));
			endPos = hit.point;
		}

		AudioManager.instance.PlaySFXOnce(AudioNames.Vampire_Dash, 6);

		m_dashTween = transform.DOMove(endPos, dashTime * portion).Play();

		yield return new WaitForSeconds(dashTime * portion);

        //Recovery

        m_AttackHitbox.gameObject.SetActive(false);
        m_Collider.enabled = true;

        //Desactiver Sign
        m_Animator.SetBool("isDashing", false);

        yield return new WaitForSeconds(m_AttackRecoveryTime);

        m_pathfinder.CanMove = true;

        m_nextAttackTime = Time.time + Random.Range(m_MinAttackCD, m_MaxAttackCD);

        isAttacking = false;
    }

    protected override void Death()
    {
        if (m_attackCoroutine != null)
        {
            StopCoroutine(m_attackCoroutine);

			m_AttackHitbox.gameObject.SetActive(false);
			m_Collider.enabled = true;
			
			//Desactiver Sign
			m_Animator.SetBool("isDashing", false);

			m_pathfinder.CanMove = true;
			
			m_nextAttackTime = Time.time + Random.Range(m_MinAttackCD, m_MaxAttackCD);
			
			isAttacking = false;
        }

        if (m_Cerceuil != null && m_Cerceuil.gameObject.activeInHierarchy)
        {
            StartCoroutine(Respawn());

            m_OnRespawn?.Invoke();
        }
        else
        {
            base.Death();
        }
    }

	[Button, TitleGroup("Enemy")]
	public override void Lit()
	{
		if(!isRespawning)
		{
			base.Lit();
		}
	}

    IEnumerator Respawn()
    {
		isRespawning = true;

		if (m_attackCoroutine != null)
		{
			StopCoroutine(m_attackCoroutine);
		}

		isAttacking = false;

        m_pathfinder.CanMove = false;
		m_canAttack = false;

		m_dashTween?.Kill();
        m_rbgd.velocity = Vector3.zero;

        IS_FLAMED = false;

        transform.position = m_Cerceuil.m_RespawnPosition.position;
		transform.rotation = m_Cerceuil.m_RespawnPosition.rotation;

        HP = MAX_HP;

        yield return new WaitForSeconds(m_RespawnTime);

		m_canAttack = true;
        m_pathfinder.CanMove = true;
		isRespawning = false;
    }
}