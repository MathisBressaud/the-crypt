﻿using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System.Collections;
using DG.Tweening;

public class Enemy : Entity, IFlamable
{
    //References
    [SerializeField, TitleGroup("Enemy")] protected Animator m_Animator;
    protected EnemyPathfinder m_pathfinder;
    protected Rigidbody m_rbgd;

    //Variables
    [SerializeField, TitleGroup("Enemy")] private float m_AttackRange = 2f;
    [SerializeField, TitleGroup("Enemy")] protected float m_MinAttackCD = 2f;
    [SerializeField, TitleGroup("Enemy")] protected float m_MaxAttackCD = 4f;
    [SerializeField, TitleGroup("Enemy")] private float m_AttackFreezeDuration = 1f;
    protected float m_nextAttackTime = 0;
    private bool m_isFreezed;

    [Space]
    [SerializeField, TitleGroup("Enemy")] private float m_StunDuration = 1f;
    [SerializeField, TitleGroup("Enemy")] private float m_BumpMultiplier = 1f;
    private bool m_isStunned;
    protected bool m_isInTransition;

    [Space]
    [SerializeField, TitleGroup("Enemy"), Range(0,100)] private float m_StoicChancePercentage = 5;

    private float m_nextFirePropagationTick = 0;
    private float m_nextFireDamageTick = 0;

    //Public bools
    public bool PlayerInAttackRange
    {
        get
        {
			return (Vector3.Distance(transform.position, m_pathfinder.m_Destination.position) <= m_AttackRange && Mathf.Abs(transform.position.y - m_pathfinder.m_Destination.position.y) < 0.5f && MapManager.instance.FindFloorStateAtPosition(transform.position) == FloorState.playable);
        }
    }

    [SerializeField, TitleGroup("Enemy")] private bool m_isFlamable = true;

    private bool is_Flamed;
    public bool IS_FLAMED
    {
        get { return is_Flamed; }
        set
        {
            m_FlameFX.SetActive(value);

            m_nextFirePropagationTick = Time.time + Constants.k_FirePropagationRate;
            m_nextFireDamageTick = Time.time + Constants.k_FireDamageRate;

            if (value && !is_Flamed)
            {
				AudioManager.instance.PlaySFXOnce(AudioNames.Fire_Start, 7);

                float rng = Random.Range(0, 100);

                if (rng >= m_StoicChancePercentage)
                {
                    m_canAttack = !value;
                    m_pathfinder.IsFlamed = value;
                }
            }
            else if (!value && is_Flamed)
            {
                m_canAttack = !value;
                m_pathfinder.IsFlamed = value;
            }

            is_Flamed = value;
        }
    }

    protected bool m_canAttack = true;

    //Debug FX
    [Space]
	[SerializeField, TitleGroup("Enemy")] private ParticleSystem m_HitFX;
    [SerializeField, TitleGroup("Enemy")] private GameObject m_FlameFX;

	//Events
	[SerializeField, TitleGroup("Enemy")] private UnityEvent m_OnDamagedIgnoreFire;

    protected override void Start()
    {
        base.Start();

        m_rbgd = GetComponent<Rigidbody>();
    }

	private void OnEnable()
	{
		//Debug
		m_pathfinder = GetComponent<EnemyPathfinder>();

		HP = MAX_HP;

		m_nextAttackTime = Time.time + Random.Range(m_MinAttackCD, m_MaxAttackCD);

		m_pathfinder.CanMove = true;
		m_isStunned = false;
		m_isFreezed = false;
	}

    protected virtual void Update()
    {
		
		if(!m_isInTransition && MapManager.instance.TimeFreeze)
		{
			StartCoroutine(Transition());
		}

        if (m_isFreezed || m_isStunned || m_isInTransition)
        {
            m_pathfinder.CanMove = false;
        }
        else
        {
            if (!m_pathfinder.CanMove)
            {
                m_pathfinder.CanMove = true;
            }

			m_rbgd.isKinematic = true;

            if(m_pathfinder.CanMove && m_pathfinder.m_agent.velocity.magnitude > 0.1f)
            {
                m_Animator.SetBool("isRunning", true);
                m_Animator.SetFloat("Speed", Mathf.InverseLerp(0, m_pathfinder.m_agent.speed, m_pathfinder.m_agent.velocity.magnitude));
            }
            else
            {
                m_Animator.SetBool("isRunning", false);
                m_Animator.SetFloat("Speed", 0f);
            }

            if (m_nextAttackTime < Time.time && PlayerInAttackRange && m_canAttack)
            {
                Attack();
            }
        }

        if(IS_FLAMED && !m_isInTransition)
        {
            FlamedBehavior();
        }
    }

    protected IEnumerator Transition()
    {
        m_isInTransition = true;

        float animatorSpeed = m_Animator.speed;
        m_Animator.speed = 0;

        m_pathfinder.m_agent.enabled = false;

		bool canAttack = m_canAttack;
		m_canAttack = false;

        yield return new WaitUntil(() => !MapManager.instance.TimeFreeze);

		m_canAttack = canAttack;

		m_pathfinder.m_agent.enabled = (MapManager.instance.FindFloorStateAtPosition(transform.position) == FloorState.playable);

        m_Animator.speed = animatorSpeed;

        m_isInTransition = false;
    }

    protected virtual void Attack()
    {
		m_isFreezed = true;

        StartCoroutine(Freeze());

        transform.DOLookAt(m_pathfinder.m_Destination.position, 0.1f).Play();

        //Anim
        m_Animator.SetTrigger("Attack");

        m_nextAttackTime = Time.time + Random.Range(m_MinAttackCD, m_MaxAttackCD);
    }

    IEnumerator Freeze() //Don't Move !
    {
        m_isFreezed = true;

        yield return new WaitForSeconds(m_AttackFreezeDuration);

        m_isFreezed = false;
    }

    public override void GetDamage(int amount, Vector3 direction, float force)
    {
        StartCoroutine(Stun());

		m_OnDamagedIgnoreFire?.Invoke();

        if (HP - amount > 0)
		{
            m_HitFX.Play();
        	Bump(direction.normalized, force);
		}
		else
		{
			GameObject m_HitFX_temp = Instantiate(m_HitFX.gameObject, m_HitFX.transform.position, m_HitFX.transform.rotation, null);
            m_HitFX_temp.GetComponent<ParticleSystem>().Play();
			Destroy(m_HitFX_temp, 5f);
		}

        //Anim
        m_Animator.SetTrigger("isHurt");
        ScreenShake.instance.TwerkMoiCetteCam();

        base.GetDamage(amount, direction, force);
    }

    private void Bump(Vector3 direction, float force)
    {
		m_rbgd.isKinematic = false;
        m_rbgd.AddForce(direction.normalized * force * m_BumpMultiplier);
    }

    IEnumerator Stun()
    {
        m_isStunned = true;

        yield return new WaitForSeconds(m_StunDuration);

        m_isStunned = false;
    }

    [Button, TitleGroup("Enemy")]
    public virtual void Lit()
    {
        if(m_isFlamable)
        {
             IS_FLAMED = true;
        }
    }

    public void FlamedBehavior()
    {
        //Propagation
        if (Time.time >= m_nextFirePropagationTick)
        {
            var collidersInRange = Physics.OverlapSphere(transform.position, Constants.k_FirePropagationRange, LayerMask.GetMask(Constants.k_LayerHitable));

            for(int i = 0; i < collidersInRange.Length; i++)
            {
                IFlamable flamable = collidersInRange[i].GetComponent<IFlamable>();

                if(flamable != null)
                {
                    float rng = Random.Range(0, 100);

                    if(rng < Constants.k_FirePropagationChance)
                    {
                        flamable.Lit();
                    }
                }
            }

            m_nextFirePropagationTick = Time.time + Constants.k_FirePropagationRate;
        }

        //Damage
        if (Time.time >= m_nextFireDamageTick)
        {
            m_lastForce = 0f;
            m_Animator.SetTrigger("isHurt");

            HP -= Constants.k_FireDamage;

            m_nextFireDamageTick = Time.time + Constants.k_FireDamageRate;
        }
    }

    protected override void Death()
    {
        //Anim
		StopAllCoroutines();
		
        base.Death();
    }
}
