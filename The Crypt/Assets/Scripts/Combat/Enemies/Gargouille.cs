﻿using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class Gargouille : Enemy
{
    private PlayerVision m_playerVision;

	[Space]
	[SerializeField, TitleGroup("Gargouille")] private UnityEvent m_OnFreeze;
	[SerializeField, TitleGroup("Gargouille")] private UnityEvent m_OnUnfreeze;
	[SerializeField, TitleGroup("Gargouille")] private UnityEvent m_OnDontDamage;

	private bool m_isFreezedVision;

    public bool IsVisible
    {
        get
        {
            return m_playerVision.CanSee(transform.position) || Vector3.Distance(transform.position, m_playerVision.transform.position) < 0.3f;
        }
    }

    protected override void Start()
    {
        m_playerVision = FindObjectOfType<PlayerVision>();

        base.Start();
    }

    protected override void Update()
    {
        if(!IsVisible)
        {
            m_Animator.speed = 1;

			if(m_isFreezedVision)
			{
				m_OnUnfreeze?.Invoke();
				m_isFreezedVision = false;
			}

            base.Update();
        }
        else
        {
            m_pathfinder.CanMove = false;

            m_Animator.speed = 0;
            m_Animator.SetBool("isRunning", false);
            m_Animator.SetFloat("Speed", 0f);

			if(!m_isFreezedVision)
			{
				m_OnFreeze?.Invoke();
				m_isFreezedVision = true;
			}

			if(!m_isInTransition && MapManager.instance.TimeFreeze)
			{
				StartCoroutine(Transition());
			}
        }
    }

    public override void GetDamage(int amount, Vector3 direction, float force)
    {
        if (PlayerSkills.instance.Skills == Skills.pickAxe || PlayerSkills.instance.Skills == Skills.Torch)
        {
            base.GetDamage(amount, direction, force);
        }
		else
		{
			m_OnDontDamage?.Invoke();
		}
    }
}
