﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyPathfinder : MonoBehaviour
{
    //References
    [HideInInspector]
    public Transform m_Destination;

    [HideInInspector]
    public NavMeshAgent m_agent;

    //Variables
    [SerializeField] private float m_AggressiveSpeed = 5;
    [SerializeField] private float m_AggressiveAngularSpeed = 175;
    [SerializeField] private float m_DetectionRange = 10;
    [SerializeField] private float m_LosingRange = 20;

    [Space]
    [SerializeField] private float m_IdleSpeed = 2;
    [SerializeField] private float m_IdleAngularSpeed = 120;
    [SerializeField] private float m_IdleWanderRange = 1;
    [SerializeField] private float m_IdleMinWaitTime = 2;
    [SerializeField] private float m_IdleMaxWaitTime = 4;
    private float m_nextIdleChangeDirection = 0;
    private Vector3 m_idleStartPosition;

    [Space]
    [SerializeField] private float m_FlameSpeed = 5;
    [SerializeField] private float m_FlameAngularSpeed = 120;
    [SerializeField] private float m_FlameWanderRange = 3;
    [SerializeField] private float m_FlameMinWaitTime = 1;
    [SerializeField] private float m_FlameMaxWaitTime = 3;
    private float m_nextFlameChangeDirection = 0;

    //Public bools
    [HideInInspector]
    public bool CanMove = false;
    [HideInInspector]
    public bool IsFlamed = false;

    private bool playerDetected = false;
    public bool PlayerDetected
    {
        get
        {
            if (MapManager.instance.FindFloorStateAtPosition(transform.position) != FloorState.playable)
            {
                playerDetected = false;
                return playerDetected;
            }

            float dist = Vector3.Distance(transform.position, m_Destination.position);

            if (!playerDetected && dist <= m_DetectionRange)
            {
                playerDetected = true;
            }
            else if (playerDetected && dist > m_LosingRange)
            {
                playerDetected = false;
            }

            return playerDetected;
        }
    }

    //States
    private bool isAggressive = false;
    private bool isIdleWandering = false;
    private bool isFlameWandering = false;

    void Start()
    {
        m_agent = GetComponent<NavMeshAgent>();

        GameObject tempDest = GameObject.FindGameObjectWithTag("Player");

        if(tempDest == null)
            Debug.LogError("No game object found with the tag Player");

        m_Destination = tempDest.transform;
    }

    private void Update()
    {
		if(m_agent.enabled && MapManager.instance.FindFloorStateAtPosition(transform.position) == FloorState.playable)
		{
        	if(CanMove)
        	{
            	if(IsFlamed)
            	{
            	    WanderFlame();
            	}
            	else if (PlayerDetected)
            	{
                	Move();
            	}
            	else
       	    	{
            	    WanderIdle();
        	    }
        	}
        	else
        	{
        	    Stop();
        	}
		}
    }

    private void Move()
    {
        if (isAggressive == false)
        {
            m_agent.speed = m_AggressiveSpeed;
            m_agent.angularSpeed = m_AggressiveAngularSpeed;

            isAggressive = true;
            isIdleWandering = false;
            isFlameWandering = false;
        }

		if(m_agent.enabled)
		{
			m_agent.isStopped = false;

			m_agent.SetDestination(m_Destination.position);
		}
    }

    private void WanderIdle()
    {
        if (isIdleWandering == false)
        {
            m_agent.speed = m_IdleSpeed;
            m_agent.angularSpeed = m_IdleAngularSpeed;
            m_idleStartPosition = transform.position;

            isAggressive = false;
            isIdleWandering = true;
            isFlameWandering = false;
        }

		if(m_agent.enabled)
		{
        	m_agent.isStopped = false;

        	if (Time.time >= m_nextIdleChangeDirection)
        	{
       		    Vector2 randomDir = Random.insideUnitCircle.normalized;
        	    m_agent.SetDestination(m_idleStartPosition + new Vector3(randomDir.x, 0, randomDir.y) * m_IdleWanderRange);
        	    m_nextIdleChangeDirection = Time.time + Random.Range(m_IdleMinWaitTime, m_IdleMaxWaitTime);
        	}
		}
    }

    private void WanderFlame()
    {
        if (isFlameWandering == false)
        {
            m_agent.speed = m_FlameSpeed;
            m_agent.angularSpeed = m_FlameAngularSpeed;

            isAggressive = false;
            isIdleWandering = false;
            isFlameWandering = true;
        }

		if(m_agent.enabled)
		{
			m_agent.isStopped = false;

        	if (Time.time >= m_nextFlameChangeDirection)
        	{
        	    Vector2 randomDir = Random.insideUnitCircle.normalized;
        	    m_agent.SetDestination(transform.position + new Vector3(randomDir.x, 0, randomDir.y) * m_FlameWanderRange);
        	    m_nextFlameChangeDirection = Time.time + Random.Range(m_FlameMinWaitTime, m_FlameMaxWaitTime);
        	}
		}
    }

    private void Stop()
    {
		if(m_agent.enabled)
		{
			m_agent.isStopped = true;
		}
    }
}
