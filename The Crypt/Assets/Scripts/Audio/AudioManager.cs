﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;

    [SerializeField] AudioLibrary audioLibrary;

    [SerializeField] AudioSource SfxSource;

    [SerializeField] AudioSource musicSource;

    public AudioSource MusicSource
    {
        get { return musicSource; }
        set { musicSource = value; }
    }

    AudioSource[] audioSources = new AudioSource[10];

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i] = CopySource(SfxSource);
        }
    }

    public void PlaySFX (string name, float fadeDelay, float fadeTime = .5f, Ease ease = Ease.InQuad)
    {
        AudioSource source = CopySource(SfxSource);
        PlaySFXWithSource(name, source);
        FadeAudioSource(source, fadeDelay, fadeTime, ease);
    }

    public AudioSource PlaySFXLoop(string name, float fadeTime = 0, Ease ease = Ease.InOutQuad)
    {
        AudioSource source = CopySource(SfxSource);
        PlaySFXWithSource(name, source);

        if (fadeTime != 0)
            FadeInAudioSource(source, GetClipByName(name).volume, fadeTime, ease);

        source.loop = true;
        return source;
    }

    public void PlaySFX(string name)
    {
        SFX sfx = GetClipByName(name);
        SfxSource.PlayOneShot(sfx.clip, sfx.volume);
    }

    public void PlaySFXOnce(string name, int index)
    {
        if (index >= audioSources.Length)
        {
            Debug.LogError("audioSource index out of range");
            return;
        }

        AudioSource source = audioSources[index];
        SFX sfx = GetClipByName(name);
        if (!source.isPlaying)
            SfxSource.PlayOneShot(sfx.clip, sfx.volume);
    }

    public void PlaySFX(string[] names)
    {
        int rng = Random.Range(0, names.Length);

        string name = names[rng];

        PlaySFX(name);
    }

    Coroutine currentLoop;
    public AudioSource PlaySFXLoop(string[] names)
    {
        int rng = Random.Range(0, names.Length);

        string name = names[rng];

        AudioSource source = CopySource(SfxSource);
        PlaySFXWithSource(name, source);

        if (currentLoop != null)
            StopCoroutine(currentLoop);

        currentLoop = StartCoroutine(NextLoop(GetClipByName(name).clip.length, names, source));

        return source;
    }

    public void PlaySFXLoop(string[] names, AudioSource source)
    {
        int rng = Random.Range(0, names.Length);

        string name = names[rng];

        PlaySFXWithSource(name, source);

        if (currentLoop != null)
            StopCoroutine(currentLoop);

        currentLoop = StartCoroutine(NextLoop(GetClipByName(name).clip.length, names, source));
    }

    IEnumerator NextLoop(float timeToWait, string[] names, AudioSource source)
    {
        yield return new WaitForSeconds(timeToWait);

        PlaySFXLoop(names, source);
    }

    public void StopLoop(AudioSource source)
    {
        if (currentLoop != null)
            StopCoroutine(currentLoop);

       // Destroy(source);
    }

    public void PlaySFXPitched(string name, float a_pitch, float amplitude = 0)
    {
        a_pitch = a_pitch + Random.Range(-amplitude / 2, amplitude / 2);
        AudioSource source = CopySource(SfxSource);
        source.pitch = a_pitch;
        PlaySFXWithSource(name, source);
        Destroy(source, GetClipByName(name).clip.length);
    }

    public Music PlayMusic(string name)
    {
        Music music = GetMusicByName(name);
        MusicSource.Stop();
        MusicSource.clip = music.clip;
        MusicSource.volume = music.volume;
        MusicSource.loop = true;
        MusicSource.Play();
        return music;
    }

    public Music PlayMusic(Music music)
    {
        MusicSource.Stop();
        MusicSource.clip = music.clip;
        MusicSource.volume = music.volume;
        MusicSource.loop = true;
        MusicSource.Play();
        return music;
    }

    public void PlaySFXWithSource(string name, AudioSource a_source)
    {
        SFX sfx = GetClipByName(name);
        a_source.clip = sfx.clip;
        a_source.volume = sfx.volume;
        a_source.Play();
    }

    public void FadeAudioSource(AudioSource source, float fadeDelay = 0, float fadeTime = .5f, Ease ease = Ease.InQuad)
    {
        StartCoroutine(Delay());

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(fadeDelay);

            DOTween.To(() => source.volume, x => source.volume = x, 0, fadeTime).Play().SetEase(ease).OnComplete(() => Destroy(source));
        }
    }

    public void FadeInAudioSource(AudioSource source, float endVolume = 1, float fadeTime = .5f, Ease ease = Ease.InOutQuad)
    {
        source.volume = 0;
        DOTween.To(() => source.volume, x => source.volume = x, endVolume, fadeTime).Play().SetEase(ease);
    }

    public SFX GetClipByName(string name)
    {
        for (int i = 0; i < audioLibrary.soundEffects.Length; i++)
        {
            if (audioLibrary.soundEffects[i].clip.name == name)
               return audioLibrary.soundEffects[i];
        }

        Debug.Log("There is no clip named " + name);
        return null;
    }

    public Music GetMusicByName(string name)
    {
        for (int i = 0; i < audioLibrary.musics.Length; i++)
        {
            if (audioLibrary.musics[i].clip.name == name)
                return audioLibrary.musics[i];
        }

        Debug.Log("There is no music named " + name);
        return null;
    }

    AudioSource CopySource(AudioSource a_audioSource)
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.outputAudioMixerGroup = a_audioSource.outputAudioMixerGroup;
        audioSource.playOnAwake = a_audioSource.playOnAwake;
        audioSource.priority = a_audioSource.priority;
        audioSource.volume = a_audioSource.volume;
        audioSource.pitch = a_audioSource.pitch;

        return audioSource;
    }
}