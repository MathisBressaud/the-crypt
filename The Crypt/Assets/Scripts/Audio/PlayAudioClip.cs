﻿using UnityEngine;
using System.Collections;

public class PlayAudioClip : MonoBehaviour
{
    [SerializeField] bool m_playOnStart;

    [SerializeField] bool m_loop;

    [SerializeField] string m_clip;

    [SerializeField] string[] m_clips;

    AudioSource loopingSource;

    private void OnEnable()
    {
        if (m_playOnStart)
            Play();
    }

    public void Play()
    {
        if (m_loop)
            PlayLoop();
        else
            PlayOnce();
    }

    public void PlayOnce()
    {
        if (!string.IsNullOrEmpty(m_clip))
            AudioManager.instance.PlaySFX(m_clip);


        if (m_clips.Length > 0)
            AudioManager.instance.PlaySFX(m_clips);
    }

    public void PlayLoop()
    {
        if (!string.IsNullOrEmpty(m_clip))
            loopingSource = AudioManager.instance.PlaySFXLoop(m_clip);


        if (m_clips.Length > 0)
            loopingSource = AudioManager.instance.PlaySFXLoop(m_clips);
    }

    public void StopLoop()
    {
        loopingSource?.Stop();

        if (m_clips.Length > 0)
            AudioManager.instance.StopLoop(loopingSource);
    }
}
