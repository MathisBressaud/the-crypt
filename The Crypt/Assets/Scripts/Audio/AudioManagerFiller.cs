﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System.IO;

public class AudioManagerFiller : MonoBehaviour
{
    [SerializeField] AudioLibrary m_library;

    [SerializeField] AudioClip[] m_clips;

    List<SFX> newSFX;

    [Button]
    public void FillSFX()
    {
        List<SFX> newSFX = new List<SFX>();

        for (int i = 0; i < m_clips.Length; i++)
        {
            newSFX.Add(new SFX(m_clips[i], 1));
        }

        m_library.soundEffects = newSFX.ToArray();
    }

    [PropertySpace]
    [Button]
    public void FillConstant()
    {
        string audioNamesString = "public static class AudioNames \n { \n";

        for (int i = 0; i < m_clips.Length; i++)
        {
            string currentAudioName = "    public const string " + m_clips[i].name + " = " + "\"" + m_clips[i].name + "\";";
            audioNamesString += currentAudioName + "\n";
        }

        audioNamesString += "}";

        File.WriteAllText(Application.persistentDataPath + "/AudioNames.cs", audioNamesString);
        DebugFilePosition();
    }

    [ContextMenu("DebugFilePosition")]
    public void DebugFilePosition()
    {
        Debug.Log(Application.persistentDataPath + "/AudioNames.cs");
    }
}
