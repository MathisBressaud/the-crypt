﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AudioLibrary")]
public class AudioLibrary : ScriptableObject
{
    [Header("Sound Effects")]
    public SFX[] soundEffects;
    public Music[] musics;
}

[System.Serializable]
public class SFX
{
    public AudioClip clip;
    [Range(0,1)]
    public float volume = 1;

    public SFX (AudioClip clip, float volume)
    {
        this.clip = clip;
        this.volume = volume;
    }
}

[System.Serializable]
public class Music
{
    public AudioClip clip;
    [Range(0, 1)]
    public float volume = 1;
    public float tempo = 130;
}