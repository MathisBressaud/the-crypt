public static class AudioNames 
 { 
    public const string Cave_Ambiance = "Cave_Ambiance";
    public const string Chandelier_Fall = "Chandelier_Fall";
    public const string Cimetierre_Ambiance = "Cimetierre_Ambiance";
    public const string Fire_Constant = "Fire_Constant";
    public const string Fire_Start = "Fire_Start";
    public const string Gargouille_Freeze = "Gargouille_Freeze";
    public const string Hit_Pierre = "Hit_Pierre";
    public const string Nope = "Nope";
    public const string PlayerHit = "PlayerHit";
    public const string Rock_Break = "Rock_Break";
    public const string Roulade = "Roulade";
    public const string Spawner = "Spawner";
    public const string SwordSlash_1 = "SwordSlash_1";
    public const string SwordSlash_2 = "SwordSlash_2";
    public const string SwordSlash_3 = "SwordSlash_3";
    public const string Transition_Floor = "Transition_Floor";
    public const string Vampire_Dash = "Vampire_Dash";
    public const string Vampire_Load = "Vampire_Load";
    public const string Zombie_Vampire_Hit = "Zombie_Vampire_Hit";
}