﻿using UnityEngine;

interface IHealth
{
    int HP { get; set; }
    int MAX_HP { get; set; }
    void GetDamage(int amount, Vector3 direction, float force);
}

interface IFlamable
{
    bool IS_FLAMED { get; set; }
    void Lit();
    void FlamedBehavior();
}

interface ICombat
{
    Side SIDE { get; }
}

public enum Side
{
    Ally, Enemy, Object
}