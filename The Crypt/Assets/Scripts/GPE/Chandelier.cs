﻿using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class Chandelier : MonoBehaviour
{
    //References
    private Rigidbody m_rbgd;

    //Variables
    [Space]
    [SerializeField] private float m_FlameRange = 5;
    [SerializeField] private float m_DamageRange = 3;
    [SerializeField] private int m_Damage = 1;
    [SerializeField] private float m_Force = 10;

    private bool m_isFalling;
    private bool m_hasFalled;

    //Events
    [Space]
    [SerializeField] private UnityEvent m_OnFallStart;
    [SerializeField] private UnityEvent m_OnCollision;

    private void Start()
    {
        m_rbgd = GetComponent<Rigidbody>();
    }

    [Button]
    public void Fall()
    {
        if(!m_isFalling && !m_hasFalled)
        {
            m_isFalling = true;
            m_rbgd.isKinematic = false;
            m_rbgd.useGravity = true;

            m_OnFallStart?.Invoke();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        m_hasFalled = true;
        m_isFalling = false;

        m_rbgd.isKinematic = true;
        m_rbgd.useGravity = false;

        m_OnCollision?.Invoke();

        //Damage
        var collidersInDamageRange = Physics.OverlapSphere(transform.position, m_DamageRange, LayerMask.GetMask(Constants.k_LayerHitable));

        for(int i = 0; i < collidersInDamageRange.Length; i++)
        {
            IHealth health = collidersInDamageRange[i].GetComponent<IHealth>();
            if (health != null)
            {
                health.GetDamage(m_Damage, transform.position - collidersInDamageRange[i].transform.position, m_Force);
            }
        }

        //Flame
        var collidersInFlameRange = Physics.OverlapSphere(transform.position, m_FlameRange, LayerMask.GetMask(Constants.k_LayerHitable));

        for (int i = 0; i < collidersInFlameRange.Length; i++)
        {
            IFlamable flamable = collidersInFlameRange[i].GetComponent<IFlamable>();
            if(flamable != null)
            {
                flamable.Lit();
            }
        }
    }
}
