﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    [Range(-1, 1)]
    public int dir;
    public Ladder connectedLadder;
    
    
    bool used;
    public bool Used
    {
        get { return used; }
        set
        {
            if (used == value)
                return;

            if(value)
                StartCoroutine(Delay());

            used = value;

            IEnumerator Delay()
            {
                yield return new WaitForSeconds(2);
                Used = false;
            }
        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || Used)
            return;

        MapManager.instance.SwitchFloor(dir);
        MapManager.instance.ValidateRespawn(connectedLadder);
        Used = connectedLadder.Used = true;
    }

}
