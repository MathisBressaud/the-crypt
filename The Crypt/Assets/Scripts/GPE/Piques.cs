﻿using UnityEngine;
using UnityEngine.Events;

public class Piques : MonoBehaviour
{
    [SerializeField] private GameObject m_Hitbox;

    [SerializeField] private bool m_AlwaysActive;

    private bool m_active;

    [SerializeField] private float m_ActiveTime = 2f;
	[SerializeField] private float m_DesactiveTime = 2f;

    private float m_timeToSwitch = 0;

    [SerializeField] private UnityEvent m_OnSwitch;

    private void Start()
    {
        if (m_AlwaysActive)
            m_Hitbox.SetActive(true);
        else
            m_timeToSwitch = Time.time + m_ActiveTime;
    }

    private void Update()
    {
        if(!m_AlwaysActive && Time.time >= m_timeToSwitch)
        {
            Switch();
        }
    }

    public void Switch()
    {
        m_OnSwitch?.Invoke();
		m_timeToSwitch = Time.time + (m_Hitbox.activeInHierarchy? m_ActiveTime : m_DesactiveTime);
        m_Hitbox.SetActive(!m_Hitbox.activeInHierarchy);
    }
}
