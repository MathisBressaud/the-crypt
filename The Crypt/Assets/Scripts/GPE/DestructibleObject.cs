﻿using UnityEngine;
using UnityEngine.Events;

public class DestructibleObject : Entity
{
	[SerializeField] private UnityEvent m_OnDontDamage;

	[SerializeField] private GameObject m_DestroyedFX;

    public override void GetDamage(int amount, Vector3 direction, float force)
    {
        if (PlayerSkills.instance.Skills == Skills.pickAxe || PlayerSkills.instance.Skills == Skills.Torch)
        {
            base.GetDamage(amount, direction, force);
        }
		else
		{
			m_OnDontDamage?.Invoke();
		}
    }

	protected override void Death()
	{
		m_DestroyedFX.SetActive(true);
		m_DestroyedFX.transform.parent = null;

		base.Death();
	}
}
