﻿using UnityEngine;
using Sirenix.OdinInspector;

public class FlamableObject : Entity, IFlamable
{
    private bool is_Flamed;
    public bool IS_FLAMED
    {
        get { return is_Flamed; }
        set
        {
			if(value && !is_Flamed)
			{
				AudioManager.instance.PlaySFXOnce(AudioNames.Fire_Start, 7);
			}

            m_FlameFX.SetActive(value);

            m_nextFirePropagationTick = Time.time + Constants.k_FirePropagationRate;
            m_nextFireDamageTick = Time.time + Constants.k_FireDamageRate;

            is_Flamed = value;
        }
    }

    private float m_nextFirePropagationTick = 0;
    private float m_nextFireDamageTick = 0;

    [SerializeField] private GameObject m_FlameFX;

    private void Update()
    {
        if (IS_FLAMED)
            FlamedBehavior();
    }

    [Button, TitleGroup("Object")]
    public void Lit()
    {
        IS_FLAMED = true;
    }

    public override void GetDamage(int amount, Vector3 direction, float force)
    {

    }

    public void FlamedBehavior()
    {
        //Propagation
        if (Time.time >= m_nextFirePropagationTick)
        {
            var collidersInRange = Physics.OverlapSphere(transform.position, Constants.k_FirePropagationRange, LayerMask.GetMask(Constants.k_LayerHitable));

            for (int i = 0; i < collidersInRange.Length; i++)
            {
                IFlamable flamable = collidersInRange[i].GetComponent<IFlamable>();

                if (flamable != null)
                {
                    float rng = Random.Range(0, 100);

                    if (rng < Constants.k_FirePropagationChance)
                    {
                        flamable.Lit();
                    }
                }
            }

            m_nextFirePropagationTick = Time.time + Constants.k_FirePropagationRate;
        }

        //Damage
        if (Time.time >= m_nextFireDamageTick)
        {
            HP -= Constants.k_FireDamage;
            m_nextFireDamageTick = Time.time + Constants.k_FireDamageRate;
        }
    }
}
