﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class ScreenShake : Singleton<ScreenShake>
{
    CinemachineBasicMultiChannelPerlin perlin;

    public float amplitude = 0.03f;
    public float time = 0.14f;


    Tween shakeTween;

    private void Start()
    {
        perlin = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
            TwerkMoiCetteCam();

    }
    public void TwerkMoiCetteCam()
    {
        shakeTween.Kill();
        shakeTween = DOTween.To(() => perlin.m_AmplitudeGain, x => perlin.m_AmplitudeGain = x, amplitude, time / 2).SetEase(Ease.InQuad).Play().OnComplete(() => {
            shakeTween = DOTween.To(() => perlin.m_AmplitudeGain, x => perlin.m_AmplitudeGain = x, 0, time / 2).SetEase(Ease.InQuad).Play();
        });
    }
}
