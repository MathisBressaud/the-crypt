﻿using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour {

    //Variables;

    [SerializeField]
    GameObject mask;

    List<GameObject> pool = new List<GameObject>();

    [SerializeField]
    int poolSize = 100;


    //Initialisation
    void Awake ()
    {
        SpawnMasks ();
	}
	
	void SpawnMasks ()
    {
        //Create Masks
        for (int i = 0; i < poolSize; i++)
        {
            GameObject currentMask;
            currentMask = Instantiate(mask);
            currentMask.SetActive(false);
            pool.Add(currentMask);
        }
	}

    public GameObject GetAvailableMask()
    {
        //Check for a mask available
        foreach (GameObject go in pool)
        {
            if (!go.activeInHierarchy)
            {
                return go;
            }
        }

        //No Mask Available
        GameObject newMask;
        newMask = Instantiate(mask);
        newMask.SetActive(false);
        pool.Add(newMask);
        poolSize += 1;

        return newMask;
    }
}
