﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DelayToLoad : MonoBehaviour
{
    public float time;
    public bool isStart;

    private void Start()
    {
        StartCoroutine(Delay());

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(time);
            SceneManager.LoadScene(isStart ? Constants.k_GameScene : Constants.k_MenuScene);
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
            SceneManager.LoadScene(isStart ? Constants.k_GameScene : Constants.k_MenuScene);
    }
}
