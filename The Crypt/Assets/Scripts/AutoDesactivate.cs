﻿using UnityEngine;

public class AutoDesactivate : MonoBehaviour
{
    [SerializeField] private float m_time = 1;
    private float m_timeToDesactivate = 0;

    private void OnEnable()
    {
        m_timeToDesactivate = Time.time + m_time;
    }

    private void Update()
    {
        if (Time.time >= m_timeToDesactivate)
            gameObject.SetActive(false);
    }
}
