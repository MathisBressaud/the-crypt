﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Crystals : MonoBehaviour
{
    public List<Entity> crystaux = new List<Entity>();

    public void CrystalBreak(Entity crystal)
    {
        if(crystaux.Contains(crystal))
            crystaux.Remove(crystal);

        if (crystaux.Count <= 0)
            EndGame();
    }

    void EndGame()
    {
        SceneManager.LoadScene(Constants.k_CinematicEnd);
    }
}
