﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using InControl;

public class CharaControler : MonoBehaviour
{
    CharaInputs inputs;
    Rigidbody rb;
    Animator anim;

    [Header("Components")]
    public Transform[] dropped = new Transform[0];

    [Header("Move Param")]
    public float spd;
    public float spdRot;
    [Space(10)]
    public float manetteFactor;
    [Space(10)]
    public float spdTimeStart;
    public float spdTimeStop;

    float spdRatio;
    Tween spdRatioSecurity;

    bool isMoving;
    bool IsMoving
    {
        get { return isMoving; }
        set
        {
            if (isMoving == value)
                return;

            SetSpeedRatio(value);

            isMoving = value;
        }
    }

    Tween rotSecurity;
    Vector3 targetRot;
    Vector3 TargetRot
    {
        get { return targetRot; }
        set
        {
            if (targetRot == value)
                return;

            rotSecurity.Kill();
            rotSecurity = transform.DORotate(value, (Mathf.Abs(value.y - targetRot.y)/360) * spdRot).SetEase(Ease.InOutQuad).Play();

            targetRot = value;
        }
    }

    [Header("Dash Param")]
    public float dashForce;
    public float dashDuration;
    public float dashRecover;
    float dashRecovering;
    [Space(10)]
    public float dashToAtkRatio = 0.75f; 
    [Space(10)]
    public float invuDuration;

    bool dashRequired;
    bool DashingRequired
    {
        get { return dashRequired; }
        set
        {
            if (dashRequired == value)
                return;

            if (value && dashRecovering <= 0 && !atkInProgress)
                Dash();

            dashRequired = value;
        }
    }
    bool dashInProgress;
    float flickFromDash;

    bool isInvu;
    public bool IsInvu
    {
        get { return isInvu; }
        set
        {
            if (isInvu == value)
                return;

            if (value)
                StartCoroutine(Delay());

            IEnumerator Delay()
            {
                yield return new WaitForSeconds(invuDuration);
                IsInvu = false;
            }

            isInvu = value;
        }
    }

    [Header("Mouse Aim")]
    public float floorTransition = 1.5f;
    public LayerMask floor0;
    public LayerMask floor1;

    RaycastHit mouseHit;

    [Header("Atk System")]
    public AtkParam[] atkParams = new AtkParam[3];
    [Space(10)]
    public float globalRecover;
    public float snapDist;
    [Space(10)]
    public LayerMask hitLayer;

    bool atkRequired;
    bool AtkRequired
    {
        get { return atkRequired; }
        set
        {
            if (atkRequired == value)
                return;

            if (value && !dashInProgress && !atkInProgress)
                Atk();

            atkRequired = value;
        }
    }
    bool atkInProgress;
    float recover;

    int atkIndex;
    int AtkIndex
    {
        get { return atkIndex; }
        set
        {
            if (atkIndex == value)
                return;

            StopCoroutine(resetAtk);
            resetAtk = ResetAtkIndex();
            StartCoroutine(resetAtk);

            atkIndex = value;
        }
    }

    IEnumerator resetAtk;
    IEnumerator ResetAtkIndex()
    {
        yield return new WaitForSeconds(globalRecover);
        AtkIndex = 0;
    }


    bool isFreeze;
    public bool IsFreeze
    {
        get { return isFreeze; }
        set
        {
            if (isFreeze == value)
                return;

            if(rb)
                rb.isKinematic = value;
            if(anim)
                anim.speed = value ? 0 : 1;
            DOTween.TogglePause(gameObject);

            isFreeze = value;
        }
    }



    //***************************************************************************************************************************
    //*****************************************          Start              *****************************************************
    private void Start()
    {
        foreach (Transform t in dropped)
            t.parent = null;


        inputs = new CharaInputs();
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();

        resetAtk = ResetAtkIndex();

        Vector3 initLeft = Vector3.zero;
        initLeft.y = transform.localEulerAngles.y + 360;
        TargetRot = initLeft;
    }

    //***************************************************************************************************************************
    //*****************************************          Update             *****************************************************
    private void Update()
    {
        HeresyNoJutsu();
        Move();
    }
    private void FixedUpdate()
    {
        DashingPhysics();
    }

    void HeresyNoJutsu()
    {
        IsMoving = InputReader();
        DashingRequired = inputs.dash;
        AtkRequired = inputs.atk;

        anim.SetFloat("Blend", spdRatio);
        dashRecovering -= Time.deltaTime;
        recover -= Time.deltaTime;
        flickFromDash -= Time.deltaTime;
    }

    //***************************************************************************************************************************
    //*****************************************          Movement           *****************************************************
    bool InputReader()
    {
        return inputs.left || inputs.right || inputs.up || inputs.down;
    }

    void ResetTargetRot()
    {
        Vector3 rot = TargetedRotation();

        rotSecurity.Kill();
        rotSecurity = transform.DORotate(rot, (Mathf.Abs(rot.y - TargetRot.y) / 360) * spdRot).SetEase(Ease.InOutQuad).Play();
    }

    Vector3 TargetedRotation()
    {
        Vector3 rot = Vector3.zero;
        rot.y = Vector2.SignedAngle(new Vector2(inputs.vertical, inputs.horizontal), new Vector2(0, 1)) + (inputs.LastInputType == BindingSourceType.KeyBindingSource || inputs.LastInputType == BindingSourceType.MouseBindingSource ? 0 : manetteFactor);
        return IsMoving ? rot : TargetRot;
    }

    void SetSpeedRatio(bool value)
    {
        spdRatioSecurity.Kill();
        spdRatioSecurity = DOTween.To(() => spdRatio, x => spdRatio = x, value ? 1 : 0, value ? spdTimeStart * (1 - spdRatio) : spdTimeStop * spdRatio).SetEase(Ease.InQuad).Play();
    }

    void Move()
    {
        if (dashInProgress || atkInProgress || IsFreeze)
            return;

        TargetRot = TargetedRotation();

        Vector3 vel = rb.velocity;
        //Vector3 newVel = (inputs.LastInputType == BindingSourceType.KeyBindingSource || inputs.LastInputType == BindingSourceType.MouseBindingSource ? transform.forward : Camera.main.transform.right) * spd * spdRatio;
        Vector3 newVel = transform.forward * spd * spdRatio;
        newVel.y = vel.y;
        rb.velocity = newVel;
    }

    //***************************************************************************************************************************
    //*****************************************          DASH               *****************************************************
    void Dash()
    {
        dashRecovering = dashRecover;
        IsInvu = true;
        dashInProgress = true;

        rb.velocity = Vector3.zero;

        anim.SetTrigger("Roll");

        transform.DOKill();

		AudioManager.instance.PlaySFXOnce(AudioNames.Roulade, 9);

        rotSecurity = transform.DORotate(TargetedRotation(), 0).Play().OnComplete(() =>
        {
            StartCoroutine(Delay());

            rb.velocity = transform.forward * dashForce;
        });

        IEnumerator Delay()
        {
            yield return new WaitForSeconds(dashDuration * dashToAtkRatio);

            flickFromDash = dashDuration * (1- dashToAtkRatio);

            yield return new WaitUntil(() => inputs.atk || flickFromDash <= 0);

            dashInProgress = false;

            if (inputs.atk)
            {
                Atk();
            }
            else
            {
                spdRatio = 1;
                SetSpeedRatio(InputReader());
                ResetTargetRot();
            }

        }
    }

    void DashingPhysics()
    {
        if (!dashInProgress )
            return;

        Vector3 vel = rb.velocity;
        vel.y = Mathf.Max(0.1962f, vel.y);
        rb.velocity = vel;
    }

    //***************************************************************************************************************************
    //*****************************************          Targeting          *****************************************************
    void MouseTargetDir()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out mouseHit, Mathf.Infinity, transform.position.y > floorTransition ? floor1 : floor0))
        {
            transform.LookAt(mouseHit.point);
            Vector3 rot = transform.localEulerAngles;
            rot.x = rot.z = 0;
            transform.localEulerAngles = rot;
        }

        //physics.overlapsphere
    }

    //***************************************************************************************************************************
    //*****************************************          Atk System         *****************************************************
    void Atk()
    {
        if (PlayerSkills.instance.Skills <= Skills.none || IsFreeze)
            return;

        atkInProgress = true;

        StartCoroutine(AtkProtocol());

        IEnumerator AtkProtocol()
        {
            for(int i = 0; AtkIndex < atkParams.Length; i++)
            {
                rb.velocity = Vector3.zero;

                if (inputs.LastInputType == BindingSourceType.KeyBindingSource || inputs.LastInputType == BindingSourceType.MouseBindingSource)
                {
                    MouseTargetDir();
                }
                else
                {
                    rotSecurity.Kill();
                    rotSecurity = transform.DORotate(TargetedRotation(), 0).Play();
                }

                anim.SetInteger("AtkIndex", AtkIndex);
                anim.SetTrigger("Atk");

                //Anticipation
                yield return new WaitForSeconds(atkParams[AtkIndex].anticipation);

                //Check target
                Transform target = null;

                List<Transform> tEnemy = new List<Transform>();
                List<Transform> tStatic = new List<Transform>();
                Collider[] hitColliders = Physics.OverlapSphere(transform.position + transform.forward * atkParams[AtkIndex].range_hit /2, atkParams[AtkIndex].range_hit / 2, hitLayer);
                foreach (var hitCollider in hitColliders)
                {
                    if (Mathf.Abs(hitCollider.transform.position.y - transform.position.y) < .75f)
                    {
                        
                        if (hitCollider.GetComponent<Entity>() is Enemy)
                            tEnemy.Add(hitCollider.transform);
                        else
                            tStatic.Add(hitCollider.transform);
                    }
                }

                if(tEnemy.Count > 0)
                {
                    foreach(Transform t in tEnemy)
                        if (!target || Vector3.Distance(transform.position, t.position) < Vector3.Distance(transform.position, target.position))
                            target = t;
                }
                else if (tStatic.Count > 0)
                {
                    foreach (Transform t in tStatic)
                        if (!target || Vector3.Distance(transform.position, t.position) < Vector3.Distance(transform.position, target.position))
                            target = t;
                }


                //Hit something ?
                //Si oui: Snap into, si non: range miss
                if (target)
                {
                    transform.LookAt(target);
                    Vector3 rot = transform.localEulerAngles;
                    rot.x = rot.z = 0;
                    transform.localEulerAngles = rot;
                    transform.DOMove(target.position + Vector3.Normalize(transform.position - target.position) * snapDist, atkParams[AtkIndex].duration).SetEase(Ease.InOutQuad).Play();
                }
                else
                    transform.DOMove(transform.position + transform.forward * atkParams[AtkIndex].range_miss, atkParams[AtkIndex].duration).SetEase(Ease.InOutQuad).Play();

                yield return new WaitForSeconds(atkParams[AtkIndex].duration + atkParams[AtkIndex].minimumRecover);

                // Hit button during recovr ET n'est pas le dernier coup ==> Repeat, sinon break combo
                recover = atkParams[AtkIndex].recover;
                yield return new WaitUntil(() => inputs.dash || (inputs.atk && AtkIndex < atkParams.Length-1) || recover <= 0);

                AtkIndex++;

                AtkIndex = AtkIndex < atkParams.Length ? AtkIndex : 0;

                if (inputs.dash)
                {
                    Dash();
                    break;
                }
                else if(recover < 0)
                {
                    break;
                }


            }

            atkInProgress = false;
            //ResetTargetRot();
        }
    }

}


[System.Serializable]
public class AtkParam
{
    public float anticipation;
    [Space(10)]
    public float range_miss;
    public float range_hit;
    [Space(10)]
    public float duration;
    [Space(10)]
    public float minimumRecover;
    public float recover;
}