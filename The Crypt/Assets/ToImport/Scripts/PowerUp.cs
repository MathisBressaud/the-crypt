﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public Skills powerUp;
    public GameObject[] textToPop = new GameObject[0];
    public GameObject[] textToDepPop = new GameObject[0];

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        PickUpPowerUp();
        gameObject.SetActive(false);
    }

    public void PickUpPowerUp()
    {
        foreach(GameObject gO in textToPop)
            gO.SetActive(true);
        foreach (GameObject gO in textToDepPop)
            gO.SetActive(false);
        PlayerSkills.instance.Skills = powerUp;
    }
}


