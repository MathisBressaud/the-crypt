﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkills : Singleton<PlayerSkills>
{
    Skills skills;
    public Skills Skills
    {
        get { return skills; }
        set
        {
            if (skills == value)
                return;

            //triggers l'anim
            foreach (Renderer render in Up[(int)value].visualMeshes)
                render.enabled = true;
            foreach (GameObject gO in Up[(int)value].visualAssets)
                gO.SetActive(true);

            dmg += Up[(int)value].dmgGain;

            skills = value;
        }
    }

    [Header("Visual Upgrades")]
    public Upgrades[] Up;

    [Header("Stats")]
    public int dmg;
    public float knockBackForce;
}

[System.Serializable]
public enum Skills
{
    none,
    stick,
    pickAxe,
    Torch
}

[System.Serializable]
public class Upgrades
{
    public Renderer[] visualMeshes;
    public GameObject[] visualAssets;
    public int dmgGain;
}