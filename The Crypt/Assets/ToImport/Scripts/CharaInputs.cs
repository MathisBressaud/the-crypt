﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class CharaInputs : PlayerActionSet
{
    public PlayerAction left;
    public PlayerAction right;
    public PlayerAction up;
    public PlayerAction down;

    public PlayerAction dash;
    public PlayerAction atk;

    public PlayerAction pause;

    public PlayerOneAxisAction horizontal;
    public PlayerOneAxisAction vertical;


    public CharaInputs()
    {
        left = CreatePlayerAction("left");
        right = CreatePlayerAction("right");
        up = CreatePlayerAction("up");
        down = CreatePlayerAction("down");

        dash = CreatePlayerAction("dash");
        atk = CreatePlayerAction("atk");

        pause = CreatePlayerAction("pause");

        horizontal = CreateOneAxisPlayerAction(left, right);
        vertical = CreateOneAxisPlayerAction(up, down);

        //*** Keyboard
        left.AddDefaultBinding(Key.Q);
        left.AddDefaultBinding(Key.A);
        right.AddDefaultBinding(Key.D);
        up.AddDefaultBinding(Key.Z);
        up.AddDefaultBinding(Key.W);
        down.AddDefaultBinding(Key.S);

        dash.AddDefaultBinding(Key.Space);
        atk.AddDefaultBinding(Mouse.LeftButton);

        pause.AddDefaultBinding(Key.Escape);


        //*** Remote
        left.AddDefaultBinding(InputControlType.LeftStickLeft);
        right.AddDefaultBinding(InputControlType.LeftStickRight);
        up.AddDefaultBinding(InputControlType.LeftStickUp);
        down.AddDefaultBinding(InputControlType.LeftStickDown);

        dash.AddDefaultBinding(InputControlType.Action1);
        dash.AddDefaultBinding(InputControlType.Action2);

        atk.AddDefaultBinding(InputControlType.Action3);
        atk.AddDefaultBinding(InputControlType.Action4);

        pause.AddDefaultBinding(InputControlType.Command);

    }
}
