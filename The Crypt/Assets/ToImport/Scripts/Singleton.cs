﻿using UnityEngine;


public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    [Header("Singleton")]
    public bool persistent;

    public static T instance;

    protected virtual void Awake()
    {
        if (instance == null)
        {
            instance = gameObject.GetComponent<T>();
            if (persistent)
                DontDestroyOnLoad(this.gameObject);
        }
        else
            Destroy(this.gameObject);
    }
}